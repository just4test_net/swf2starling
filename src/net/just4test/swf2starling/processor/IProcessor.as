package net.just4test.swf2starling.processor
{
	/**
	 *类处理器 
	 * @author just4test
	 * 
	 */
	public interface IProcessor
	{
		/**
		 *处理给定类，并返回其实现代码
		 * @param c
		 * @return 
		 * 
		 */
		function p(c:Class):void;
		
		/**
		 *指定该处理器绑定的类或父类。 
		 * @return 
		 * 
		 */
		function get classNames():Vector.<String>;	
		
		/**
		 *指定该处理器绑定的前缀。
		 * @return 
		 * 
		 */
		function get prefixes():Vector.<String>;
		
	}
}