package net.just4test.swf2starling.expoter.code
{
	use namespace AS3;
	
	/**
	 *支持缩进的代码块
	 * @author just4test
	 * 
	 */
	public class CodeBlock
	{
		public function CodeBlock(...args)
		{
			push.apply(this, args);
		}
		
		private var _codes:Vector.<CodeLine> = new Vector.<CodeLine>;
		
		/**
		 *默认缩进 
		 */
		public var defaultIndent:int;
		
		/**
		 *在代码块尾部添加一条代码 
		 * @param code 代码文本
		 * @param indent 缩进。如果不指定将使用默认缩进
		 * 
		 */
		public function add(code:String, indent:int = int.MIN_VALUE):void
		{
			var line:CodeLine = new CodeLine;
			line.code = code;
			line.indent = int.MIN_VALUE == indent ? defaultIndent : indent;
			_codes.push(line);
		}
		
		/**
		 *在代码块尾部推入多条代码 
		 * @param args 一个数组。其中的每一个元素要么是代码文本字符串，要么是[代码文本, 缩进]数组。
		 * 
		 */
		public function push(...args):void
		{
			for each(var i:* in args)
			{
				if(i is Array && 2 == i.length)
					add(i[0], i[1]);
				else
					add(null == i ? "" : i.toString());
			}
		}
		
		/**
		 * 连接另一个代码块到当前代码块尾部
		 * @param b 要连接的代码块
		 * @param offset 代码块的缩进偏移。如果不指定，将使用默认缩进。
		 * 
		 */
		public function concat(b:CodeBlock, offset:int = int.MIN_VALUE):void
		{
			if(int.MIN_VALUE == offset)
				offset = defaultIndent;
			
			for each(var i:CodeLine in b._codes)
			{
				var tmp:CodeLine = new CodeLine;
				tmp.code = i.code;
				tmp.indent = Math.max(0, i.indent + offset);
				_codes.push(tmp);
			}
		}
		
		public function offset(value:int):void
		{
			for each(var i:CodeLine in _codes)
			{
				i.indent = Math.max(0, i.indent + offset);
			}
		}
		
		public function code(index:int):CodeLine
		{
			return _codes[index]
		}
		
		public function toString():String
		{
			var tmp:Vector.<String> = new Vector.<String>(_codes.length);
			for(var i:int = _codes.length - 1; i >= 0; i--)
				tmp[i] = _codes[i].toString();
			
			return tmp.join("\n");
		}
		
		public static function creatByDoc(asDoc:String):CodeBlock
		{
			var ret:CodeBlock = new CodeBlock;
			
			if(Boolean(asDoc))
			{
				var arr:Array = asDoc.split("\n");
				for(var i:int = 0; i < arr.length; i++)
					arr[i] = " * " + arr[i];
				arr.unshift("/**");
				arr.push("**/");
				ret.push.apply(ret, arr);
			}
			return ret;
		}
		
	}
}
class CodeLine
{
	private static const tabs:Vector.<String> = new Vector.<String>;
	
	{
		tabs[0] = "";
		//不会有超过16层的缩进吧
		while(tabs.length < 16)
			tabs[tabs.length] = tabs[tabs.length - 1] + "    ";
	}
	
	public var code:String;
	public var indent:int;
	
	public function toString():String
	{
		return code ? tabs[indent] + code : "";//空字串不缩进
	}
}