package net.just4test.swf2starling.expoter.code
{
	/**
	 *引用管理器 
	 * @author just4test
	 * 
	 */
	public class Imports
	{
		private var _map:Object = {};
		
		/**
		 *创建一个引用集合 
		 * @param args 提供任意多个参数。这些参数可以是：
		 * <br>String。字符串本身将作为引用的路径
		 * <br>References。该引用将添加到当前引用。
		 * <br>I$。该对象的reference属性将添加到当前引用。
		 * 
		 */
		public function Imports(...args)
		{
			for each(var x:* in args)
			{
				add(x);
			}
		}
		
		public function add(x:*):void
		{
			if(x is String)
			{
				_map[x] = true;
				return;
			}
			
			var ref:Imports = x as Imports;
			if(x is I$)
				ref = x.reference;
			if(!x)
				return;
			
			for(var i:String in ref._map)
			{
				_map[i] = true;
			}
		}
		
		/**
		 *转换为代码集合 
		 * @param pkg 指定当前包名
		 */
		public function toCode(currentPkg:String):CodeBlock
		{
			var ret:CodeBlock = new CodeBlock;
			var a:Array = [];
			for(var x:String in _map)
			{
				a.push(x);
			}
			a.sort();
			
			for each(x in a)
			{
				var shortName:String;
				var pkg:String;
				var topPkg:String;
				var laseTopPkg:String;
				x = x.replace("::", ".");
				var index:int = x.indexOf(".");
				if(index != -1)
				{
					shortName = x.substr(index + 1);
					pkg = x.substr(0, index);
				}
				else
				{
					continue;//顶级包，无需引用
				}
				
				if(pkg == currentPkg)
					continue;
				
				topPkg = pkg.split(".").shift();
				if(laseTopPkg && laseTopPkg != topPkg)
				{
					ret.add("");
				}
				laseTopPkg = topPkg;
				
				ret.add("import " + x + ";");
			}
			return ret;
		}
	}
}