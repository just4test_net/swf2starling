package net.just4test.swf2starling
{
	import flash.utils.getQualifiedClassName;
	
	import net.just4test.swf2starling.expoter.code.$Class;
	import net.just4test.swf2starling.processor.IProcessor;

	public class ClassInfo
	{
		private var _c:Class;
		public var $class:$Class;
		public var p:IProcessor;
		public const reference:Array = [];
		
		public function ClassInfo(c:Class)
		{
			_c = c;
		}
		
		public function get c():Class
		{
			return _c;
		}
		
		public function get name():String
		{
			return getQualifiedClassName(c);
		}
	}
}