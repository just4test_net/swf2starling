package net.just4test.swf2starling.expoter.code
{
	public interface I$
	{
		/**
		 *对象名
		 */
		function get $name():String;
		function set $name(value:String):void;
		
		/**
		 *对象的ASDoc 
		 */
		function get $doc():String;
		function set $doc(value:String):void;
		
		/**
		 *指示对象类型或返回值
		 */
		function get $type():String;
		function set $type(value:String):void;
		
		/**
		 * 指示对象的命名空间。包括空、public、protected、private、internal及其他用户自定义值。
		 */		
		function get $namespace():String;
		function set $namespace(value:String):void;
		
		/**
		 *返回对对象的引用
		 * @return 
		 */
		function get $references():Imports;
		
		/**
		 *导出代码块，同时可指定一些参数
		 * @param name 指定对象的name。这在I$被多处引用时很有用
		 * @param args
		 * @return 
		 * 
		 */
		function $toCode(name:String = null, ...args):CodeBlock;
	}
}