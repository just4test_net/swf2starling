package net.just4test.swf2starling.expoter.code
{
	/**
	 *$Function专用的参数序列结构体。因为使用内部类作为Vector类型会导致运行时错误，所以提出来作为单独的文件。 
	 * @author Just4test
	 * 
	 */
	internal class ArguData
	{
		private var _name:String;
		private var _type:String;
		private var _defaultValue:String;
		
		public function ArguData(name:String = null, type:String = null, defaultValue:String = null)
		{
			_name = name;
			_type = type;
			_defaultValue = defaultValue;
		}
		
		
		
		public function get name():String
		{
			return _name || "value";
		}
		
		public function set name(value:String):void
		{
			_name = value;
		}
		
		public function get type():String
		{
			return _type;
		}
		
		public function set type(value:String):void
		{
			_type = value || "*";
		}
		
		public function get defaultValue():String
		{
			return _defaultValue;
		}
		
		public function set defaultValue(value:String):void
		{
			_defaultValue = value;
		}
		
		public function toString():String
		{
			return name + (_type ? ":" + type : "") + (_defaultValue ? " = " + _defaultValue : "");
		}
	}
}