package net.just4test.util
{
	import flash.events.Event;
	
	public class EventWithData extends Event
	{
		protected var _data:*;
		
		public function EventWithData(type:String, data:*, bubbles:Boolean=false)
		{
			super(type, bubbles, true);
			_data = data;
		}
		
		public function get data():*
		{
			return _data;
		}
		
		override public function clone():Event
		{
			var ret:EventWithData = new EventWithData(type, data, bubbles);
			return ret;
		}
	}
}