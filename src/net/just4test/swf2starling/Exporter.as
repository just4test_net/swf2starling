package net.just4test.swf2starling
{
	import flash.system.ApplicationDomain;
	
	import net.just4test.swf2starling.processor.ProcessorCtrl;

	public class Exporter
	{
		private static var _domain:ApplicationDomain;
		
		
		/**
		 *核心方法。将各个类映射到其实现与处理器 
		 */
		public static function process(domain:ApplicationDomain):void
		{
			_domain = domain;
			
			var classNames:Vector.<String> = _domain.getQualifiedDefinitionNames();
			for each(var i:String in classNames)
			{
				trace("---类",i);
				try
				{
					var c:Class = _domain.getDefinition(i) as Class;
				} 
				catch(error:Error) 
				{
					Log.logf("反射名为[{0}]的类时发生错误：{1}", i, error);
					continue;
				}
				
				if(!c)
				{
					Log.logf("名为[{0}]的对象不是类，而是 {1}", i, _domain.getDefinition(i));
					continue;
				}
				
				ProcessorCtrl.processClass(c);
				
			}
		}
	
	}
}