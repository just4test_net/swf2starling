package
{
	import flash.desktop.Clipboard;
	import flash.desktop.ClipboardFormats;
	import flash.desktop.NativeDragActions;
	import flash.desktop.NativeDragManager;
	import flash.display.Sprite;
	import flash.events.NativeDragEvent;
	import flash.filesystem.File;
	import flash.system.Worker;
	import flash.utils.describeType;
	
	import net.just4test.swf2starling.Core;
	import net.just4test.swf2starling.display.MotionClip;
	import net.just4test.swf2starling.expoter.code.$Class;
	import net.just4test.swf2starling.expoter.code.$Function;
	import net.just4test.swf2starling.expoter.code.$Object;
	import net.just4test.util.multithread.ThreadManager;
	
	public class SWF2Straling extends Sprite
	{	
		public function SWF2Straling()	
		{
			MotionClip;
			if (!Worker.current.isPrimordial)
				return;
			
			ThreadManager.init(stage, Workers.net_just4test_util_multithread_ThreadManager);
			ThreadManager.threadLimit = 2;
			ThreadManager.regClass("test", TestThread);
			ThreadManager.call("test", null, null, 0, "哒哒啦");
			ThreadManager.call("test", function(...args):void{trace("返回值", args);}, null, 0);
			ThreadManager.call("test", null, null, 0);
			ThreadManager.call("test", null, null, 0);
			ThreadManager.call("test", null, null, 0);
			ThreadManager.call("test", null, null, 0);
			
//			new $Object;
//			trace((new $Object)["constructor"])
//			var c:$Class = new $Class("aaaaa", "bbbbb");
//			c["test"] = new $Function();
//			
//			
//			trace(c.$toCode())
//			
//			var s:Sprite = new Sprite;
//			addChild(s);
//			s.graphics.beginFill(0xffff00);
//			s.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
//			s.graphics.endFill();
//
//			stage.addEventListener(NativeDragEvent.NATIVE_DRAG_ENTER, nativeDragEnterHandler);
//			stage.addEventListener(NativeDragEvent.NATIVE_DRAG_DROP, nativeDragDropHandler);
		}
		
		private function nativeDragEnterHandler(event:NativeDragEvent):void
		{
			if(getSwfList(event.clipboard)){ 
				NativeDragManager.acceptDragDrop(this);
				NativeDragManager.dropAction = NativeDragActions.COPY;
			} 
		}
		
		private function nativeDragDropHandler(event:NativeDragEvent):void
		{
			Core.loadFiles(getSwfList(event.clipboard));
		}
		
		private function getSwfList(clipboard:Clipboard):Vector.<File>
		{
			var array:Array = clipboard.getData(ClipboardFormats.FILE_LIST_FORMAT) as Array;
			var ret:Vector.<File> = new Vector.<File>;
			for each(var i:File in array)
			{
				var ext:String = i.extension.toLocaleLowerCase();
				if("swf" == ext || "swc" == ext)
					ret.push(i);
			}
			
			return ret.length ? ret : null;
		}
		
		
	}
}