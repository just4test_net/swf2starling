package net.just4test.swf2starling.processor
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.utils.getQualifiedClassName;
	
	import net.just4test.swf2starling.expoter.code.$Class;

	public class SpriteProcessor implements IProcessor
	{
		public function SpriteProcessor()
		{
		}
		
		public function p(c:Class):void
		{
			trace("类被传送到SpriteProcessor处理器",c);
			try
			{
				var instance:Sprite = new c;
			} 
			catch(error:Error) 
			{
				trace("实例化类时发生异常。", error);
				return;
			}

			var numChildren:int = instance.numChildren;
			var childrenData:Vector.<RichDisObjData> = new Vector.<RichDisObjData>;
			var $c:$Class = new $Class(getQualifiedClassName(c));
			
			for (var i:int = 0; i < numChildren; i++) 
			{
				try
				{
					var data:RichDisObjData = new RichDisObjData(instance.getChildAt(i));	
				} 
				catch(error:Error) 
				{
					trace(error);
					continue;
				}
				childrenData.push(data);
			}
		}
		
		public function get classNames():Vector.<String>
		{
			return new <String>["flash.display::Sprite"];
		}
		
		public function get prefixes():Vector.<String>
		{
			return null;
		}
	}
}