package net.just4test.util.multithread
{
	internal class TaskData
	{
		public var name:String;
		public var successCallback:Function;
		public var failedCallback:Function;
		public var timeout:Number;
		
		public var args:Array;
	}
}