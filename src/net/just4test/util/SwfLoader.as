package net.just4test.util
{
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.system.ApplicationDomain;
	import flash.system.LoaderContext;
	import flash.utils.ByteArray;

	public class SwfLoader extends EventDispatcher
	{
		public function SwfLoader(domain:ApplicationDomain = null)
		{
			
			if(domain)
				_domain = domain;
		}
		
		private var _loader:Loader;
		private var _domain:ApplicationDomain;
		private var _files:Vector.<File>;
		public static const EVENT_FAIL:String = "EVENT_FAIL";

		public function load(files:Vector.<File>):void
		{
			if(_files && _files.length || _loader)
				throw Error("上一次的载入过程还没有完成");
			
			
			_files = files;
			loadSingle(files.shift());
		}
		
		private function loadNext():void
		{
			if(_files && _files.length)
				loadSingle(_files.shift());
			else
				dispatchEvent(new Event(Event.COMPLETE));
				
		}
		
		
		public function loadSingle(file:File):void
		{
			if(_loader)
			{
				throw new Error("上一次的载入过程还没有完成");
			}
			
			var fileStream:FileStream = new FileStream();
			fileStream.open(file, FileMode.READ);
			var bytes:ByteArray = new ByteArray;
			fileStream.readBytes(bytes);
			if("swc" == file.extension)
			{
				bytes = Utils.getSWFFromSWC(bytes);
				if(!bytes)
				{
					dispatchEvent(new EventWithData(EVENT_FAIL, "解压缩错误"));
				}
			}
			_loader = new Loader();
			var context:LoaderContext = new LoaderContext(false, _domain);
			context.allowCodeImport = true;
			_loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onLoaderCompleteHandler);
			_loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onFailHandler);
			_loader.loadBytes(bytes, context);//加载到域
		}
		private function onLoaderCompleteHandler(e:Event):void
		{
			_loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onLoaderCompleteHandler);
			_loader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, onFailHandler);
			_loader = null;
			loadNext();
		}
		private function onFailHandler(e:Event):void
		{
			_loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onLoaderCompleteHandler);
			_loader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, onFailHandler);
			_loader = null;
			var reason:String;
			switch(e.type)
			{
				case IOErrorEvent.IO_ERROR:
					reason = "IO错误";
					break
				default:
					reason = e.toString();
			}
			dispatchEvent(new EventWithData(EVENT_FAIL, reason));
		}

	}
}