package net.just4test.swf2starling.expoter.code
{
	public class $Class extends $Object implements I$
	{
		private var _supperClass:String;
		public var $constructor:$Function;
		private var _interface:Vector.<String>;
		public var $package:String = "";
		
		
		public function $Class(name:String, supperClass:String = "Object")
		{
			_supperClass = supperClass;
			$name = name;
		}
		
		
		/**
		 *存储实例方法和变量 
		 */
		public const $static:$Object = new $Object;
		
		override public function $toCode(name:String = null, ...args):CodeBlock
		{
			var ret:CodeBlock = $references.toCode($package);
			ret.concat(CodeBlock.creatByDoc($doc));
			
			ret.push(
				Log.formatStr(
					"{0}class {1}{2}{3}",
					$namespace ? $namespace + " " : "",
					$name,
					"Object" != _supperClass ? " extends " + _supperClass : "",
					_interface && _interface.length ? " implements " + _interface.join(",") : ""
				));
				
			ret.add("{");
			
			if($constructor)
			{
				$constructor.$name = $name;
				$constructor.$type = null;
				ret.concat($constructor.$toCode(), 1);
			}
			
			for(var name:String in this)
			{
				ret.concat(this[name].$toCode(name), 1);
			}
			
			for(name in $static)
			{
				var temp:CodeBlock = $static[name].$toCode(name);
				temp.code(0).code += "static ";
				ret.concat(temp, 1);
			}
			
			ret.add("}");
			
			return ret;
		}
		
		override public function toString():String
		{
			return "[$Class " + $name + "]";
		}
		
		override public function get $references():Imports
		{
			var ret:Imports = new Imports;
			
			for each(var i$:I$ in this)
			{
				ret.add(i$.$references);
			}
			
			return ret;
		}
	}
}