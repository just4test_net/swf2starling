package net.just4test.swf2starling.expoter.code
{
	import flash.utils.Proxy;
	import flash.utils.flash_proxy;
	
	/**
	 *可以安插其他I$的对象。 
	 * @author just4test
	 * 
	 */
	public dynamic class $Object extends Proxy implements I$
	{
		private var _name:String;
		private var _shadowMap:Object = {};
		private var _shadowNames:Vector.<String>;
		private var _reference:Imports = new Imports;
		private var _namespace:String;
		
		public function $Object()
		{
		}
		
		public function get $name():String
		{
			return _name;
		}
		
		public function get $namespace():String
		{
			// TODO Auto Generated method stub
			return _namespace;
		}
		
		public function set $namespace(value:String):void
		{
			_namespace = value;
		}
		
		public function get $references():Imports
		{
			// TODO Auto Generated method stub
			return _reference;
		}
		
		
		public function set $name(value:String):void
		{
			_name = value;
		}
		
		public function get $doc():String
		{
			return null;
		}
		
		public function set $doc(value:String):void
		{
		}
		
		public function get $type():String
		{
			// TODO Auto Generated method stub
			return null;
		}
		
		public function set $type(value:String):void
		{
			// TODO Auto Generated method stub
			
		}
		
		//////////////////////////////////////////////////////////
		
		override flash_proxy function getProperty(name:*):*
		{
			name = String(name);
			trace("获取属性", name);
			return _shadowMap[name];
		}
		
		override flash_proxy function hasProperty(name:*):Boolean
		{
			name = String(name);
			return _shadowMap[name];
		}
		
		override flash_proxy function setProperty(name:*, value:*):void
		{
			name = String(name);
			if(!value is I$)
				throw Error("ObjShadow只能接受I$");
			_shadowMap[name] = value;
		}

		override flash_proxy function nextNameIndex (index:int):int {
			// initial call
			if (index == 0) {
				_shadowNames = new Vector.<String>;
				for (var x:String in _shadowMap) {
					_shadowNames.push(x);
				}
			}
			
			if (index < _shadowNames.length) {
				return index + 1;
			} else {
				return 0;
			}
		}
		override flash_proxy function nextName(index:int):String {
			return _shadowNames[index - 1];
		}
		
		override flash_proxy function nextValue(index:int):*
		{
			// TODO Auto Generated method stub
			return _shadowMap[_shadowNames[index - 1]];
		}
		
		
		///////////////////////////////////////////////////////////
		
		public function toString():String
		{
			return "[$Object " + $name + "]";
		}
		
		public function getSubName(target:I$):String
		{
			for(var s:String in _shadowMap)
			{
				if(target == _shadowMap[s])
					return s;
			}
			return null;
		}
		
		public function $toCode(name:String = null, ...args):CodeBlock
		{
			return null;
		}
	}
}