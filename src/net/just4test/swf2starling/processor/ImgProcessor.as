package net.just4test.swf2starling.processor
{
	import flash.display.BitmapData;
	import flash.utils.getQualifiedClassName;
	
	import feathers.controls.Callout;
	
	import net.just4test.swf2starling.expoter.BmpHandle;

	public class ImgProcessor implements IProcessor
	{
		public function ImgProcessor()
		{
		}
		
		public function p(c:Class):void
		{
			trace("类被传送到ImgProcessor处理器",c);
			var data:BitmapData = new c;
			BmpHandle.regBmp(data).className = getQualifiedClassName(c);
		}
		
		public function get classNames():Vector.<String>
		{
			BitmapData
			return new <String>["flash.display::BitmapData", "flash.display::Bitmap"];
		}
		
		public function get prefixes():Vector.<String>
		{
			return new <String>["img_"];
		}
	}
}