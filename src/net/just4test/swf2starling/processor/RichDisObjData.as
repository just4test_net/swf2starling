package net.just4test.swf2starling.processor
{
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	
	import net.just4test.swf2starling.ClassInfo;
	import net.just4test.swf2starling.display.DisObjData;
	import net.just4test.swf2starling.expoter.code.CodeBlock;

	/**
	 *表示单个显示对象在某一帧的状态。这个对象将被序列化,并反序列化为其父类。
	 * <br>
	 * 
	 */
	public class RichDisObjData extends DisObjData
	{
		
		protected var _children:Vector.<RichDisObjData>;
		protected var _parent:RichDisObjData;
		protected var _classInfo:ClassInfo;
		
		public function RichDisObjData(dis:DisplayObject = null)
		{
			if(dis)
				init(dis);
		}
		
		public function init(dis:DisplayObject):void
		{
			x = dis.x;
			y = dis.y;
			scaleX = dis.scaleX;
			scaleY = dis.scaleY;
			rotation = dis.rotation;
			alpha = dis.alpha;
			visible = dis.visible;
			
			if(dis is DisplayObjectContainer)
				_children = new Vector.<RichDisObjData>;
		}

		public function get children():Vector.<RichDisObjData>
		{
			return _children.concat();
		}
		
		public function addChild(disData:RichDisObjData):void
		{
			if(disData._parent)
				throw "搞毛？"
				
			_children.push(disData);
			disData._parent = this;
			
		}
		
		public function get recursiveScaleX():Number
		{
			return _parent ? _parent.recursiveScaleX : scaleX;
		}
		
		public function get recursiveScaleY():Number
		{
			return _parent ? _parent.recursiveScaleY : scaleY;
		}
		
		public function toCode(name:String, parentName:String = null):CodeBlock
		{
			var ret:CodeBlock = new CodeBlock;
			
			if(x)
				ret.add(name + ".x = " + x);
			if(y)
				ret.add(name + ".y = " + y);
			if(1 != scaleX)
				ret.add(name + ".scaleX = " + scaleX);
			if(1 != scaleY)
				ret.add(name + ".scaleY = " + scaleY);
			if(rotation)
				ret.add(name + ".rotation = " + rotation);
			if(1 != alpha)
				ret.add(name + ".alpha = " + alpha);
			if(!visible)
				ret.add(name + ".visible = " + visible);
			
			ret.add((parentName ? parentName + "." : "") + "addChild(" + name + ")");
			
			return ret;
		}

		public function get classInfo():ClassInfo
		{
			return _classInfo;
		}
		
		
	}
}