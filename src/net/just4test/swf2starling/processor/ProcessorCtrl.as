package net.just4test.swf2starling.processor
{
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Loader;
	import flash.utils.Dictionary;
	import flash.utils.describeType;
	import flash.utils.getQualifiedClassName;
	
	import net.just4test.swf2starling.ClassInfo;

	public class ProcessorCtrl
	{
		private static var _classMap:Dictionary = new Dictionary;
		private static const _processors:Vector.<IProcessor> = new Vector.<IProcessor>;
		private static const _catchers:Vector.<ICatcher> = new Vector.<ICatcher>;
		private static const _classNameMap:Object = {};
		private static const _prefixes:Vector.<String> = new Vector.<String>;
		private static const _prefixeMap:Object = {};
		
		{
			regProcessor(new SpriteProcessor);
			regProcessor(new ImgProcessor);
		}
		
		/**
		 *处理类 
		 */
		public static function processClass(c:Class):ClassInfo
		{
			if(_classMap[c])
				return _classMap[c];
			
			var info:ClassInfo = new ClassInfo(c);
			_classMap[c] = info;
			trace("处理类", info.name);
			try
			{
				info.p = typeInference(c);
				if(info.p)
					info.p.p(c);
				else
					trace("因为没有找到处理器，抛弃该类");
			} 
			catch(error:Error) 
			{
				Log.logf("处理名为[{0}]的类时发生错误：{1}", info.name, error);
				return null;
			}
			
			return info;
		}
		
		/**
		 *处理对象 
		 */
		public static function processObj(target:Object):ClassInfo
		{
			var c:Class = target["constructor"] as Class;
			if(_classMap[c])
				return _classMap[c];
			
			trace("~~~~", c)
			
			//如果是某些特殊类型，比如补间动画，自身没有转化为类的意义
			if(isNativeClass(c))
				return null;
			
			var className:String = getQualifiedClassName(target);
			scanChild(target);
			
			return processClass(c);
			
			function scanChild(target:Object):void
			{
				var container:DisplayObjectContainer = target as DisplayObjectContainer;
				if(!container || container is Loader)
					return;
				
				var length:int = container.numChildren;
				for(var i:int = 0; i < length; i++)
				{
					var dis:DisplayObject = container.getChildAt(i);
					trace("===================", getQualifiedClassName(dis));
					processObj(dis);
					
				}
			}
		}
		
		/**
		 *指示对象是否是一个原生类。原生类对象意味着类本身不能和外观对应。原生类的处理方式是生成截图。
		 */
		private static function isNativeClass(c:Class):Boolean
		{
			var name:String = getQualifiedClassName(c);
			if(0 == name.indexOf("flash.display::"))
				return true;
			
			return false;
		}
		
		
		/**
		 *类型推断
		 * <br>1.如果任何实现了ICatcher的处理器捕获了这个类，则使用该处理器。
		 * <br>2.如果类名匹配任何处理器已注册的前缀，则匹配了最长的前缀的处理器将被使用
		 * <br>3.依次判断给定类的基类。直到发现了任何已注册的基类。
		 * @param c 指定需要推断的类
		 * @return
		 */
		private static function typeInference(c:Class):IProcessor
		{
			for each(var ic:ICatcher in _catchers)
			{
				if(ic.willHandle(c))
					return ic;
			}
			
			var name:String = getQualifiedClassName(c);
			
			var shortName:String = name.split("::").pop();
			var prefixeLength:int;
			for each(var i:String in _prefixes)
			{
				if(shortName.indexOf(i) == 0 && i.length > prefixeLength)
				{
					prefixeLength = i.length;
				}
				if(prefixeLength)
					return _prefixeMap[shortName.substr(0, prefixeLength)];
			}
			
			var xml:XML = describeType(c);
			for each(xml in xml.factory.extendsClass)
			{
				name = xml.@type;
				
				if(_classNameMap[name])
					return _classNameMap[name];
			}
			
			return null;
		}
		
		public static function regProcessor(p:IProcessor):void
		{
			_processors.push(p);
			
			if(p is ICatcher)
				_catchers.push(p as ICatcher);
			
			for each(var i:String in p.classNames)
			{
				_classNameMap[i] = p;
			}
			
			for each(i in p.prefixes)
			{
				if(_prefixeMap[i])
				{
					throw Error("两个处理器指定了相同的前缀！");
				}
				_prefixes.push(i);
				_prefixeMap[i] = p;
			}
		}
	}
}