package net.just4test.swf2starling.expoter.code
{
	public class $Function implements I$
	{
		private var _name:String = "func";
		private var _doc:String;
		private var _type:String = "void";
		private var _namespace:String = "";
		private var _reference:Imports = new Imports;
		
		public function $Function()
		{
		}
		
		public function get $name():String
		{
			return _name;
		}
		
		public function set $name(value:String):void
		{
			_name = value;
		}
		
		public function get $doc():String
		{
			return _doc;
		}
		
		public function set $doc(value:String):void
		{
			_doc = value;
		}
		
		public function get $type():String
		{
			return _type;
		}
		
		public function set $type(value:String):void
		{
			_type = value;
		}
		
		public function get $namespace():String
		{
			return _namespace;
		}
		
		public function set $namespace(value:String):void
		{
			_namespace = value;
		}
		
		public function get $references():Imports
		{
			return _reference;
		}
		
		public const arguments:Vector.<ArguData> = new Vector.<ArguData>;
		
		
		public var bodyCode:CodeBlock = new CodeBlock;
		
		/** 
		 * @param name 提供一个值覆盖name
		 * @return 
		 * 
		 */
		public function $toCode(name:String = null, ...args):CodeBlock
		{
			
			var ret:CodeBlock = CodeBlock.creatByDoc($doc);
			
			var argStr:String = "";
			for each(var i:ArguData in arguments)
			{
				argStr += (argStr.length ? ", " : "") + i.toString();
			}
			
			ret.push(
				Log.formatStr(
					"{0}function{1}({4}):{2}{3}",
					$namespace ? $namespace + " " : "",
					"" == name ? "" : " " + (name || $name),
					$type,
					bodyCode ? "" : ";",
					argStr
				)
			);
			
			if(bodyCode)
			{
				ret.push("{");
				ret.concat(bodyCode, 1);
				ret.push("}");
			}
			return ret;
		}
	}
}