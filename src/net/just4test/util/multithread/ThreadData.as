package net.just4test.util.multithread
{
	import flash.system.MessageChannel;
	import flash.system.Worker;

	internal class ThreadData
	{
		public var workName:String;
		public var worker:Worker;
		public var send:MessageChannel;
		public var receive:MessageChannel;
		public var task:TaskData;
		/**线程运行时，它是当前任务开始执行的时间；线程闲置时，它是进入闲置状态的时间。*/
		public var time:int;
		
		private var _index:int;
		private static const counter:Object = {};
		
		public function ThreadData()
		{
			if(!counter[workName])
				counter[workName] = 0;
			
			_index = counter[workName] ++;
		}
		

		public function toString():String
		{
			return workName + "_" + _index;
		}

	}
}