package net.just4test.util.multithread
{
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.system.ApplicationDomain;
	import flash.system.LoaderContext;
	import flash.system.MessageChannel;
	import flash.system.Worker;
	import flash.system.WorkerDomain;
	import flash.system.WorkerState;
	import flash.utils.ByteArray;
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;
	import flash.utils.getTimer;
	import flash.utils.setTimeout;

	/**
	 *使用此类需要将本类设置为嵌入的ActionScript Worker，
	 * @author Just4test
	 * 
	 */
	public class ThreadManager extends Sprite
	{
		////////////////////公共代码///////////////////////////////////
		
		private static const SUCCESS:int = 1;
		private static const FAILED:int = 2;
		private static const START_WORK:int = 3;
		
		private static function sendArgs(channel:MessageChannel, args:Array):void
		{
			var length:int = args.length;
			channel.send(length);
			for(var i:int = 0; i < length; i++)
			{
				channel.send(args[i]);
			}
		}
		
		private static function receiveArgs(channel:MessageChannel):Array
		{
			var ret:Array = [];
			var length:int = channel.receive(true);
			for(var i:int = 0; i < length; i++)
			{
				ret[i] = channel.receive(true);
			}
			return ret;
		}
		
		///////////////////主线程使用//////////////////////////
		/**可用的运行中线程数。建议设置为不大于cpu核心数 - 1。休眠中的线程不计数。*/
		public static var threadLimit:int = 4;
		/**如果线程超时，fail处理函数将收到此消息*/
		public static const FAIL_BY_TIME_OUT:String = "FAIL_BY_TIME_OUT";
		/**如果线程执行任务时崩溃，fail处理函数将收到此消息*/
		public static const FAIL_BY_CRASH:String = "FAIL_BY_CRASH";
		
		/**这个变量引用Workers的自身Embed
		 * <br>在ASC2.0（使用AIR SDK 而不是 Flex SDK）中，在Worker类内引用Worker.as中自身的绑定会报错（虽然忽略错误直接运行似乎是正常的）*/		
		//private static var managerBytes:ByteArray = Workers.net_just4test_util_multithread_ThreadManager;
		private static var managerBytes:ByteArray = null;
		private static var mainBytes:ByteArray;
		private static var _stage:Stage;
		private static const workerMap:Object = {};
		
		
		/**线程最大休眠时间，超过这个时间就会被kill掉。这个时间不能过大，因为超过15秒等待任务的语句就会超时报错。*/
		private static const MAX_IDLE_TIME:int = 10000;
		/**休眠的线程*/
		private static const idleThreads:Vector.<ThreadData> = new Vector.<ThreadData>;
		/**执行中的线程*/
		private static const runningThreads:Vector.<ThreadData> = new Vector.<ThreadData>;
		/**任务列表*/
		private static const tasks:Vector.<TaskData> = new Vector.<TaskData>;
		
		private static var stage:Stage;
		
		/**
		 * 初始化[仅在主线程有效]
		 * @param stage Stage的引用。Stage将作为主线程的心跳，并且从中获取绑定的ByteArray。
		 * @param managerBytes
		 * 
		 */
		public static function init(stage:Stage, managerBytes:ByteArray):void
		{
			if (!Worker.current.isPrimordial)
				return;
			
			mainBytes = stage.loaderInfo.bytes;
			mainBytes.shareable = true;
			
			ThreadManager.stage = stage;
			ThreadManager.managerBytes = managerBytes;
		}
		
		private static function testContest():void
		{
			if (!Worker.current.isPrimordial)
				throw Error("不得在子线程中调用此方法");
			if(!mainBytes)
				throw Error("需要先调用init进行初始化。");
		}
		
		/**
		 * 注册Worker类[仅在主线程有效]
		 * @param name 线程调用昵称
		 * @param c 线程类或者其完全限定名。注意如果传入线程类，将导致该类被引入主线程。
		 * @param bytes 指定该类的swf代码。
		 * <br>通常，您应将实现实现IThread的类指定为嵌入的 ActionScript Worker，并传递该worker的ByteArray。
		 * <br>但如果您不指定此值，将使用主程序代码。此时，您应该在您的应用程序主类的第一句写：
		 * <br>if (!Worker.current.isPrimordial) return;
		 */
		public static function regClass(name:String, c:*, bytes:ByteArray = null):void
		{
			testContest();
			
			if(c is Class)
				c = getQualifiedClassName(c);
			
			var data:ClassData = new ClassData;
			data.c = c;
			data.bytes = bytes;
			
			workerMap[name] = data;
		}
		
		/**
		 *创建线程 
		 */
		private static function crateThread(name:String):ThreadData
		{
			testContest();
			
			var data:ClassData = workerMap[name];
			if(!data)
				throw new Error("没有注册对应[" + name + "]的IThread类");
			
			var thread:ThreadData = new ThreadData;
			thread.workName = name;
			thread.worker = WorkerDomain.current.createWorker(managerBytes,true);
			thread.send = Worker.current.createMessageChannel(thread.worker);
			thread.worker.setSharedProperty("_receive", thread.send);
			thread.receive = thread.worker.createMessageChannel(Worker.current);
			thread.worker.setSharedProperty("_send", thread.receive);
			thread.worker.setSharedProperty("_bytes", data.bytes || mainBytes);
			thread.worker.setSharedProperty("_class", data.c);
			thread.worker.setSharedProperty("_toString", thread.toString());
			thread.time = getTimer();
			thread.worker.start();
			var state:int = thread.receive.receive(true);
			trace("线程[", thread, "]启动用时", getTimer() - thread.time, "毫秒。");
			if(SUCCESS == state)
			{
				return thread;
			}
			else
			{
				trace("线程启动失败。",thread.receive.receive(true));
				return null;
			}
		}
		
		/**
		 *执行一个命令[仅在主线程有效]
		 * @param name 线程调用昵称
		 * @param successCallBack 调用成功的回调
		 * @param failedCallBack 调用失败的回调
		 * @param timeout 超时时间。指定0或者负数将永不超时
		 * @param args 命令的参数
		 */
		public static function call(name:String, successCallback:Function, failedCallback:Function, timeout:int, ...args):void
		{
			testContest();
			
			var data:ClassData = workerMap[name];
			if(!data)
				throw Error("没有注册对应[" + name + "]的IThread类");
			
			var task:TaskData = new TaskData;
			task.name = name;
			task.successCallback = successCallback;
			task.failedCallback = failedCallback;
			task.timeout = timeout;
			task.args = args;
			
			tasks.push(task);
			startTasks();
			
			stage.addEventListener(Event.ENTER_FRAME, enterFrameHandler);
		}
		
		/**
		 * 真正的执行一个命令
		 * @param task
		 * 
		 */
		private static function startTask(task:TaskData):Boolean
		{
			for (var i:int = 0; i < idleThreads.length; i++) 
			{
				var thread:ThreadData = idleThreads[i];
				if(WorkerState.TERMINATED == thread.worker.state)
				{
					trace("线程[", thread, "]崩溃了");
					var callback:Function = thread.task.failedCallback;
					//callback && callback.apply(null, FAIL_BY_CRASH);
					callback && setTimeout.apply(null, [callback, 1, FAIL_BY_CRASH]);
					runningThreads.splice(runningThreads.indexOf(thread), 1);
					continue;
				}
				if(thread.workName == task.name)
				{
					idleThreads.splice(idleThreads.indexOf(thread), 1);
					break;
				}
				thread = null;
			}
			
			if(!thread)
			{
				thread = crateThread(task.name);
				if(!thread)
				{
					task.failedCallback && task.failedCallback("创建线程失败");
					return false;
				}
			}
			trace("线程[", thread, "]开始执行任务");
			
			runningThreads.push(thread);
			thread.send.send(START_WORK);
			sendArgs(thread.send, task.args);
			
			thread.task = task;
			
			return true;
		}
		
		private static function startTasks():void
		{
			while(tasks.length && runningThreads.length < threadLimit)
			{
				var task:TaskData = tasks[0];
				if(startTask(tasks[0]))
					tasks.shift();
				else
					return;
			}
		}
		
		private static function enterFrameHandler(event:Event):void
		{
			
			//检查运行中的线程是否已经完成任务或超时
			var current:int = getTimer();
			for each(var thread:ThreadData in runningThreads.concat())
			{
				if(WorkerState.TERMINATED == thread.worker.state)
				{
					trace("线程[", thread, "]崩溃了");
					callback = thread.task.failedCallback;
					//callback && callback.apply(null, FAIL_BY_CRASH);
					callback && setTimeout.apply(null, [callback, 1, FAIL_BY_CRASH]);
					runningThreads.splice(runningThreads.indexOf(thread), 1);
					continue;
				}
					
				var state:int = thread.receive.receive();
				if(state)
				{
					switch(state)
					{
						case 0:
							break;
						
						case SUCCESS:
							var args:Array = receiveArgs(thread.receive);
							var callback:Function = thread.task.successCallback;
							//callback && callback.apply(null, args);
							callback && setTimeout.apply(null, [callback, 1].concat(args));
							break;
						
						case FAILED:
							callback = thread.task.failedCallback;
							//callback && callback.apply(null, thread.receive.receive(true));
							callback && setTimeout.apply(null, [callback, 1, thread.receive.receive(true)]);
							break;
							
					}
					
					runningThreads.splice(runningThreads.indexOf(thread), 1);
					idleThreads.push(thread);
					thread.time = current;
					trace("线程[", thread, "]完成任务，休眠了");
				}
				else if(thread.task.timeout > 0 && thread.time + thread.task.timeout < current)
				{
					trace("线程[", thread, "]因为超时被杀死");
					thread.worker.terminate();
					runningThreads.splice(runningThreads.indexOf(thread), 1);
					callback = thread.task.failedCallback;
					//callback && callback.apply(null, TIME_OUT);
					callback && setTimeout.apply(null, [callback, 1, FAIL_BY_TIME_OUT]);
				}
			}
			
			startTasks();
			
			//移除闲置线程
			while(idleThreads.length)
			{
				thread = idleThreads[0];
				if(thread.time + MAX_IDLE_TIME > current)
					break;
				
				trace("线程[", thread, "]因为闲置过久被杀死");
				thread.worker.terminate();
				idleThreads.splice(idleThreads.indexOf(thread), 1);
			}
			
			if(!idleThreads.length && !runningThreads.length && !tasks.length)
			{
				stage.removeEventListener(Event.ENTER_FRAME, enterFrameHandler);
				trace("所有任务已经处理。线程管理器休眠。");
			}
		}
		
		
		
		//////////////////////////////子线程使用///////////////////////////////////////
		private static var _receive:MessageChannel;
		private static var _send:MessageChannel;
		private static var _thread:IThread;
		private static var _toString:String;
		private static var _isRunning:Boolean;
		private static var _current:ThreadManager;
		
		
		public function ThreadManager()
		{
			if (Worker.current.isPrimordial)
			{
				throw new Error("不得自行创建实例");
			}
			else
			{
				if(_current)
					throw new Error("不得自行创建实例");
				
				_current = this;
				
				_receive = Worker.current.getSharedProperty("_receive");
				_send = Worker.current.getSharedProperty("_send");
				_toString = Worker.current.getSharedProperty("_toString");
				var loader:Loader = new Loader;
				loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onLoaderCompleteHandler);
				loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onFailHandler);
				var bytes:ByteArray = Worker.current.getSharedProperty("_bytes");
				
				var byteNoShare:ByteArray = new ByteArray;
				byteNoShare.writeBytes(bytes);
				var context:LoaderContext = new LoaderContext(false, ApplicationDomain.currentDomain);
				context.allowCodeImport = true;
				loader.loadBytes(byteNoShare, context);
			}
		}
		
		private function onLoaderCompleteHandler(e:Event):void
		{
			var loaderInfo:LoaderInfo = e.target as LoaderInfo;
			loaderInfo.removeEventListener(Event.COMPLETE, onLoaderCompleteHandler);
			loaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, onFailHandler);
//			loaderInfo.loader.unload();
			
			try
			{
				var c:Class = getDefinitionByName(Worker.current.getSharedProperty("_class")) as Class;
				_thread = new c;
				_send.send(SUCCESS);
			} 
			catch(error:Error) 
			{
				_send.send(FAILED);
				_send.send("创建IThread时发生错误\n" + error);
				return;
			}
			
			getWork();
		}
		
		private function onFailHandler(e:Event):void
		{
			var loaderInfo:LoaderInfo = e.target as LoaderInfo;
			loaderInfo.removeEventListener(Event.COMPLETE, onLoaderCompleteHandler);
			loaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, onFailHandler);
			
			
			_send.send(FAILED);
			_send.send("装载类定义时发生错误\n" + e);
		}
		
		private static function getWork():void
		{
			var state:int = _receive.receive(true);
			switch(state)
			{
				case START_WORK:
					_isRunning = true;
					var args:Array = receiveArgs(_receive);
					_thread.work.apply(_thread, args);
					break;
					
				default:
					if(state)//在主线程杀掉子线程时，_receive.receive(true) 有可能返回0。
						throw Error("未知的状态码：" + state);
					break;
			}
		}
		
		/**
		 *使用此方法稍后调用getWork，以允许线程完成一些清理工作，不然线程将被阻塞。 
		 */
		private static function getWorkLater():void
		{
			setTimeout(getWork, 1);
		}
		
		/**
		 *命令执行成功，并传入参数[仅在子线程有效]
		 * <br>一旦调用此方法，调用函数返回后线程就会挂起，直到接收到下一个任务。
		 * @param args
		 * 
		 */
		public static function done(...args):void
		{
			if(!_isRunning)
				throw Error("子线程未处于执行状态，不得调用此方法");
			_isRunning = false;
			
			trace("线程[", _toString, "]回报任务完成");
			
			_send.send(SUCCESS);
			sendArgs(_send, args);
			
			getWorkLater();
		}
		
		/**
		 *命令执行失败[仅在子线程有效]
		 * <br>一旦调用此方法，调用函数返回后线程就会挂起，直到接收到下一个任务。
		 * @param msg
		 * 
		 */
		public static function fail(msg:String):void
		{
			if(!_isRunning)
				throw new Error("子线程未处于执行状态，不得调用此方法");
			_isRunning = false;
			
			trace("线程[", _toString, "]回报任务失败", msg);
			
			_send.send(FAILED);
			_send.send(msg);
			
			getWorkLater();
		}
		
		/**
		 *崩溃[仅在子线程有效]
		 * 一旦调用此方法，当前进行中的任务（如果有）就会失败；当前线程立即阻塞；线程将不会处理其他任务并被杀死。
		 */
		public static function crash():void
		{
			trace("线程[" + _toString + "]崩溃");
			
			Worker.current.terminate();
		}
	}
}

import flash.utils.ByteArray;
class ClassData
{
	public var c:String;
	public var bytes:ByteArray;
}