package net.just4test.swf2starling
{
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.system.ApplicationDomain;
	import net.just4test.util.EventWithData;
	import net.just4test.util.SwfLoader;

	public class Core
	{
		private static var _domain:ApplicationDomain;		
		public static function loadFiles(files:Vector.<File>):void
		{
			_domain = new ApplicationDomain();
			
			var loader:SwfLoader = new SwfLoader(_domain);
			loader.addEventListener(Event.COMPLETE, pluginLoadedHandler);
			loader.addEventListener(SwfLoader.EVENT_FAIL, pluginLoadFailHandler);
			loader.load(files);
		}
		
		private static function pluginLoadedHandler(e:Event):void
		{
			var loader:SwfLoader = e.target as SwfLoader;
			loader.removeEventListener(Event.COMPLETE, pluginLoadedHandler);
			loader.removeEventListener(SwfLoader.EVENT_FAIL, pluginLoadFailHandler);
			Log.log("文件载入成功。");
			Exporter.process(_domain);
		}
		
		private static function pluginLoadFailHandler(e:EventWithData):void
		{
			var loader:SwfLoader = e.target as SwfLoader;
			loader.removeEventListener(Event.COMPLETE, pluginLoadedHandler);
			loader.removeEventListener(SwfLoader.EVENT_FAIL, pluginLoadFailHandler);
			Log.log("文件载入错误。原因:", e.data);
		}
		
		
		
	}
}