package net.just4test.swf2starling.display
{
	import flash.events.Event;
	
	import starling.display.DisplayObject;
	import starling.display.Sprite;
	
	/**
	 *动作剪辑
	 * <br>相当于原生AS3的MovieClip，支持时间轴的子对象 
	 * @author Just4test
	 * 
	 */
	public class MotionClip extends Sprite
	{
		private var _data:MotionClipData
		private var _target:Vector.<DisplayObject>;
		
		private var _currentFrame:uint;
		
		public function MotionClip(data:MotionClipData)
		{
			_data = data;
			_target = new Vector.<DisplayObject>(_data.frams.length);
//			gotoAndPlay(1);
		}
		
		private function crateTarget(s:String):DisplayObject
		{
			return null;
		}
		
		public final function get totalFrames():int
		{
			return _data.frams.length;
		}
		
		
		
		private function showFrame():void
		{
			var frame:Vector.<DisObjData> = _data.frams[_currentFrame];
			var length:uint = frame.length;
			for(var i:int = 0; i < length; i ++)
			{
				var oData:DisObjData = frame[i];
				var index:int = oData.index;
				oData.applyTo(addChildAt(_target[index] ||= crateTarget(_data.targetsName[index]), i));
			}
			removeChildren(length - 1);
		}

		public function get currentFrame():uint
		{
			return _currentFrame;
		}

		public function set currentFrame(value:uint):void
		{
			if(value < 1 || value > totalFrames)
				throw new ArgumentError("指定跳转到第" + value + "帧，但总共只有" + totalFrames + "帧。");
			
			_currentFrame = value;
			
			showFrame();
		}
		
		private function enterFrameHandler(event:Event):void
		{
			_currentFrame = _currentFrame % totalFrames + 1;
			showFrame();
		}
		
		public function play():void
		{
			addEventListener(Event.ENTER_FRAME, enterFrameHandler);
		}
		
		public function stop():void
		{
			removeEventListener(Event.ENTER_FRAME, enterFrameHandler);
		}
		
		public function gotoAndPlay(frame:int):void
		{
			currentFrame = frame;
			play();
		}
		
		public function gotoAndStop(frame:int):void
		{
			currentFrame = frame;
			stop();
		}
		
		

	}
}