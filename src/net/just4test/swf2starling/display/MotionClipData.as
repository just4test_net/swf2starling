package net.just4test.swf2starling.display
{
	/**
	 *复杂影片剪辑数据
	 * <br>和Starling的图片序列影片剪辑不同，复杂影片剪辑支持在帧之间复用子对象，并随着帧的播放使这些子对象运动。
	 * @author Just4test
	 * 
	 */
	public class MotionClipData
	{
		/**指定所有帧中的子对象的构建路径*/
		public var targetsName:Vector.<String>;
		
		/**指定所有的帧，以及每一帧中用到的所有子对象的数据。*/
		public var frams:Vector.<Vector.<DisObjData>>;
	}
}