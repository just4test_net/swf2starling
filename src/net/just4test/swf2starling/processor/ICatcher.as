package net.just4test.swf2starling.processor
{
	public interface ICatcher extends IProcessor
	{
		/**
		 *该处理器能够根据给定类判断其是否能够处理。 这个判断比根据基类和前缀判断更为优先。 
		 * <br>
		 * @param c
		 * @return 如果能够处理，返回true
		 * 
		 */
		function willHandle(c:Class):Boolean;
	}
}