package net.just4test.swf2starling.display
{
	import starling.display.DisplayObject;

	/**
	 *表示单个显示对象在某一帧的状态。这个对象将被序列化/反序列化。
	 * <br>
	 * 
	 */
	public class DisObjData
	{
		public var x:Number;
		public var y:Number;
		public var scaleX:Number = 1;
		public var scaleY:Number = 1;
		public var rotation:Number;
		public var alpha:Number = 1;
		public var visible:Boolean = true;
		
		public var index:uint;
		
		
		public function applyTo(target:DisplayObject):void
		{
			
			target.x = x;
			target.y = y;
			target.scaleX = scaleX;
			target.scaleY = scaleY;
			target.rotation = rotation;
			target.alpha = alpha;
			target.visible = visible;
		}
	}
}