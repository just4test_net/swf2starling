package net.just4test.swf2starling.expoter
{
	import flash.display.BitmapData;
	import flash.display.PNGEncoderOptions;
	import flash.display.StageQuality;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	
	import net.just4test.swf2starling.processor.RichDisObjData;

	public class BmpHandle
	{
		public const _references:Vector.<RichDisObjData> = new Vector.<RichDisObjData>;
		private var _bitmapData:BitmapData;
		
		public function BmpHandle(bitmapData:BitmapData)
		{
			_bitmapData = bitmapData;
		}

		public function get width():int
		{
			return _bitmapData.width;
		}

		public function get height():int
		{
			return _bitmapData.height;
		}

		public function get bmp():BitmapData
		{
			return _bitmapData;
		}
		
		public function addReference(reference:RichDisObjData):void
		{
			_references.push(reference);
		}
		
		/**
		 *如果该BitmapData导出为类，这里指示其类名。
		 *一旦指定导出类名，
		 */
		public var className:String;
		
		
		public function get name():String
		{
			return handle2Name ? handle2Name[this] : null;
		}
		
		private function named(value:String):void
		{
			handle2Name[this] = value;
		}
		
		/**
		 *缩放比例。该比例指定了导出的图片是否经过缩小。
		 * 如果所有 
		 * @return 
		 * 
		 */
		public function get scale():Point
		{
			if(className || !_references.length)
				return new Point(1, 1);
			
			var x:int;
			var y:int;
			
			for each(var reference:RichDisObjData in _references)
			{
				x = Math.max(x, reference.recursiveScaleX);
				y = Math.max(y, reference.recursiveScaleY);
			}
				
				
			return new Point(x, y);
		}
		
		
		/**
		 *根据尺寸映射bmp，以便快速确定相同的bmp 
		 */
		private static var sizeMap:Object = {};
		
		private static var handle2Name:Dictionary;
		
		/**
		 *清除所有已经注册的bmp
		 */
		public static function clear():void
		{
			sizeMap = {};
			handle2Name = null;
		}
		
		/**
		 *引用设置完毕，为所有句柄命名。当几个图像重名时，会以下划线加数字后缀区分。
		 * <br>注意，图像名字并不稳定。更改图片或组件结构都会导致图像名字改变。需要稳定的图像名字时，请为图像指定导出名，且该导出名的类名部分不与其他图像冲突。（导出名的包名不同，但类名相同即为冲突）
		 * 
		 */
		public static function refreshName():void
		{
			var name2Vector:Object = {};
			handle2Name = new Dictionary;
			
			//将句柄分配到命名数组中
			for each(var v:Vector.<BmpHandle> in sizeMap)
			{
				for each(var handle:BmpHandle in v)
				{
					var name:String;
					
					if(handle.className)
						name = handle.className.split("::").pop();
					else if(!handle._references.length)
						name = "Img_" + handle.width + "x" + handle.height;
					else
						name = handle._references[0].classInfo.name.split("::").pop();
					
					name2Vector[name] ||= new Vector.<BmpHandle>;
					name2Vector[name].push(handle);
				}
			}
			
			//从每个命名数组中取得句柄列并为其命名
			for(name in name2Vector)
			{
				v = name2Vector[name];
				if(1 == v.length)
				{
					handle2Name[v[0]] = name;
					continue;
				}
				for(var i:int = 0; i < v.length; i++)
				{
					handle2Name[v[i]] = name + "_" + i;
					
				}
			}
		}
		
		/**
		 * 注册一个bmp并返回其句柄
		 * @param bitmapData 像素数据
		 * @param reference 对该像素数据的引用。引用有助于确定缩小倍率和导出名
		 * @return bmp句柄
		 * 
		 */
		public static function regBmp(bitmapData:BitmapData, reference:RichDisObjData = null):BmpHandle
		{
			var size:String = bitmapData.width + "x" + bitmapData.height;
			var v:Vector.<BmpHandle> = sizeMap[size];
			if(v)
			{
				for each(var handle:BmpHandle in v)
				{
					if(handle._bitmapData == bitmapData || !handle._bitmapData.compare(bitmapData))
					{
						reference && handle._references.push(reference);
						return handle;
					}
				}
			}
			else
			{
				v = new Vector.<BmpHandle>;
				sizeMap[size] = v;
			}
			
			handle = new BmpHandle(bitmapData);
			reference && handle._references.push(reference);
			v.push(handle);
			
			return handle;
		}
		
		/**
		 *在子线程中用于导出png 
		 * @param bitmapData
		 * @param scale
		 * @return 
		 * 
		 */
		public static function export(bitmapData:BitmapData, scale:Point = null):ByteArray
		{
			var scaled:BitmapData;
			if(!scale)
				scaled = bitmapData;
			else
			{
				scaled = new BitmapData(bitmapData.width, bitmapData.height);
				scaled.drawWithQuality(bitmapData, new Matrix(scale.x, 0, 0, scale.y, 0, 0), null, null, null, false, StageQuality.BEST); 
			}
			
			return scaled.encode(scaled.rect, PNGEncoderOptions);
				
		}
	}
}