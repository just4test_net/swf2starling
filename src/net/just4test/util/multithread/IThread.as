package net.just4test.util.multithread
{
	/**
	 *线程类接口
	 * <br>线程类在主线程中不会被初始化。 
	 * @author Just4test
	 * 
	 */
	public interface IThread
	{	
		function work(...args):void;
	}
}