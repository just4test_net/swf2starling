package
{
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.system.Capabilities;

	public class Log
	{
		private static var outStream:FileStream;
		private static var errStream:FileStream;
		
		{
			if(-1 != Capabilities.manufacturer.indexOf("Macintosh")
				|| -1 != Capabilities.manufacturer.indexOf("Linux"))
			{
//				outStream = new FileStream();
//				outStream.open(new File("/dev/stdout"), FileMode.WRITE);
//				errStream = new FileStream();
//				errStream.open(new File("/dev/stderr"), FileMode.WRITE);
			}
		}
		
		
		/**
		 *输出基本的调试信息。在*nix系统上，这还将输出到stdout。 
		 * 
		 */
		public static function log(...args):void
		{
			var s:String = "";
			for each(var i:String in args)
			{
				s += i + " ";
			}
			
			outStream && outStream.writeUTFBytes(s);
			trace.apply(null, args);
		}

		public static function logf(str:String, ...rest):void
		{
			trace(formatStr(str,rest));
		}
		
		/**
		 *输出错误信息。在*nix系统上，这还将输出到stderr。
		 */
		public static function error(...args):void
		{
			var s:String = "";
			for each(var i:String in args)
			{
				s += i + " ";
			}
			
			errStream && errStream.writeUTFBytes(s);
			trace.apply(null, args);
		}
		
		public static function errorf(str:String, ...rest):void
		{
			error(formatStr(str,rest));
		}
		
		/**
		 *格式化字符串。
		 *<br>format("我叫{0}。", "just4test") = "我叫just4test。" 
		 *<br>抄袭自Flex里面的 mx.utils.StringUtil.substitute
		 */
		public static function formatStr(str:String, ... rest):String
		{
			if (str == null) return '';
			
			var len:uint = rest.length;
			var args:Array;
			if (len == 1 && rest[0] is Array)
			{
				args = rest[0] as Array;
				len = args.length;
			}
			else
			{
				args = rest;
			}
			
			for (var i:int = 0; i < len; i++)
			{
				str = str.replace(new RegExp("\\{"+i+"\\}", "g"), args[i]);
			}
			
			return str;
		}
	}
}