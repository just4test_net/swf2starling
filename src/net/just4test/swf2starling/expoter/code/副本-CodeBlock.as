package net.just4test.swf2starling.expoter.code
{
	use namespace AS3;
	
	/**
	 *支持缩进的代码块
	 * <br>代码必须为String
	 * <br>虽然可以通过code[n]访问第n行的代码，但严禁直接通过code[code.length] = "xxxxx"创建一行代码。
	 * @author just4test
	 * 
	 */
	public dynamic class CodeBlock extends Array
	{
		private var _globalIndent:int = -1;

		
		private var indentPerLine:Array = [];
		
		private static const tabs:Vector.<String> = new Vector.<String>;
		
		{
			tabs[0] = "";
			//不会有超过16层的缩进吧
			while(tabs.length < 16)
				tabs[tabs.length] = tabs[tabs.length - 1] + "	";
		}
		
		public function CodeBlock()
		{
		}
		
		public function getCode(index:int):String
		{
			return tabs[getIndent(index)] + this[index];
		}
		
		public function toCode():String
		{
			var tmp:Vector.<String> = new Vector.<String>(length);
			for(var i:int = length - 1; i >= 0; i--)
				tmp[i] = getCode(i);
			
			return tmp.join("\n");
		}
		
		/**
		 *获取指定行号的缩进 
		 * @param index
		 * @return 
		 * 
		 */
		public function getIndent(index:int):int
		{
			return _globalIndent < 0 ? indentPerLine[index] : _globalIndent;
		}
		
		/**
		 *设置指定行号的缩进。注意全局缩进将覆盖逐行缩进。
		 * @param index
		 * @param value
		 * 
		 */
		public function setIndent(index:int, value:int):void
		{
			indentPerLine[index] = value;
		}
		
		
		/**
		 *全局缩进。当值大于等于0时表示启用全局缩进，并覆盖逐行缩进。 
		 * @return
		 */
		public function get globalIndent():int
		{
			return _globalIndent;
		}
		public function set globalIndent(value:int):void
		{
			_globalIndent = value;
		}
		
		/**
		 *调整缩进。任何一行代码的缩进不会调整到小于0. 
		 * @param value
		 * 
		 */
		public function indent(value:int):void
		{
			if(_globalIndent < 0)
			{
				for(var i:int = length; i >= 0; i--)
				{
					indentPerLine[i] += value;
					indentPerLine[i] = indentPerLine[i] < 0 ? 0 : indentPerLine[i];
				}
			}
			else
			{
				_globalIndent += value;
				_globalIndent = _globalIndent < 0 ? 0 : _globalIndent;
			}
			
			concat
		}
		
		///////////////////////////////////////////////
		
		/**
		 *连接代码块/单个代码。
		 * 注意连接后的代码块将不使用全局缩进。
		 * @param args
		 * @return 
		 * 
		 */
		override AS3 function concat(...args):Array 
		{ 
			var ret:CodeBlock = new CodeBlock;
			args.unshift(this);
			
			for each (var i:* in args)
			{
				if(i is CodeBlock)
				{
					ret.superPush.apply(ret, i);
					ret.indentPerLine.push.apply(ret, i.indentPerLine);
				}
				else
				{
					ret.push(i);
				}
			}
			return ret; 
		}
		
		private function superPush(...args):uint
		{
			return super.push.apply(this, args);
		}
		
		/**
		 * 允许推入[代码, 缩进]
		 * @param args
		 * @return 
		 * 
		 */
		override AS3 function push(...args):uint 
		{
			args.unshift(length, 0);
			splice.apply(this, args);
			
			return length;
		}
		
		override AS3 function pop():*
		{
			indentPerLine.pop();
			return super.pop();
		}
		
		override AS3 function shift():*
		{
			indentPerLine.shift();
			return super.shift();
		}
		
		override AS3 function unshift(...args):uint 
		{ 
			args.unshift(0, 0);
			splice.apply(this, args);
			
			return length;
		} 
		
		override AS3 function slice(A:*=0, B:*=4.294967295E9):Array
		{
			// TODO Auto Generated method stub
			return super.slice(A, B);
		}
		
		
		
		override AS3 function splice(...args):* 
		{ 
			var startIndex:int = args[0];
			var deleteCount:int = args[1];
			var iargs:Array = [startIndex, deleteCount];
			var l:int = args.length;
			for(var i:int = 2; i < l; i++)
			{
				var tar:* = args[i];
				if(tar is Array && 2 == tar.length)
				{
					iargs[i] = tar[1];
					args[i] = tar[0];
				}
				else
				{
					iargs[i] = 0;
				}
			}
			indentPerLine.splice.apply(this, iargs);
			return (super.splice.apply(this, args)); 
		}
	}
}